## __Atividade do REO 2 - 2020/02 - Inteligência Artificial - GCC128 - UFLA__ ##

---

__Professor:__ Ahmed Ali Abdalla Esmin

__Alunos:__ Nicolas Abrantes Bicalho - 10A / Arlen Mateus Mendes - 14A

---

### Para que serve este repositório? ###

É a implementação de um simples Agente Inteligente, como ele funciona, sua introdução e visão. A cada 5 segundos uma sujeira aparecerá de forma aleatória na tela, e o aspirador de pó que gira em sentido horário, fará a limpeza.

__Previa do App: __[Video](https://drive.google.com/file/d/1OTttYIqe84P8Ky8YFMAYsrv0aLajYKeK/view?usp=sharing)

![picture](Imagens/Preview.png)

### Ferramentas e Linguagem ###

* __Linguagem:__ Swift 5
* __SO:__ iOS / macOS Catalina
* __Ferramentas:__ Xcode 12.1 / Git
* __Icones:__ https://www.flaticon.com/free-icons/ios

### Como utilizar ###

* Para utilizar o programa é necessario um sistema __macOS__ (Catalina) com a ferramenta do __Xcode__ (v11+) instalada.
* Abra o projeto pelo arquivo: [RandomVaccumAgent4Places.xcodeproj](RandomVaccumAgent4Places/RandomVaccumAgent4Places.xcodeproj)
* Selecione um simulador, recomendamos o iPhone SE(2 generation)

	![picture](Imagens/Preview2.png)

* Agora é só buildar ou utilizar o comando: ``` ⌘command + R ```