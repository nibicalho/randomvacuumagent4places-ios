//
//  ViewController.swift
//  RandomVaccumAgent4Places
//
//  Created by Nícolas Bicalho on 11/11/20.
//

import UIKit

enum CurrentStatus: String {
    case dirt1
    case dirt2
    case dirt3
    case dirt4
}

class ViewController: UIViewController {
    
    // MARK: - @IBOutlet
    
    @IBOutlet weak var vacuumCleaner: UIImageView!
    @IBOutlet weak var vacuumCleanerCenterY: NSLayoutConstraint!
    @IBOutlet weak var vacuumCleanerCenterX: NSLayoutConstraint!
    @IBOutlet weak var dirt1: UIImageView!
    @IBOutlet weak var dirt2: UIImageView!
    @IBOutlet weak var dirt3: UIImageView!
    @IBOutlet weak var dirt4: UIImageView!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var viewSpaceForMoving: UIView!
    
    // MARK: - Variables
    
    var currentStatus: CurrentStatus = .dirt1
    var dirtArray: [Int] = [1,2,3,4]
    
    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.layoutIfNeeded()
        randomTimeForDirtAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        start()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    // MARK: - Private
    
    func start() {
        labelStatus.text = "Starting"
        hideDirt()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.moveRight()
        }
    }
    
    func hideDirt() {
        dirt1.isHidden = true
        dirt1.alpha = 0
        dirt2.isHidden = true
        dirt2.alpha = 0
        dirt3.isHidden = true
        dirt3.alpha = 0
        dirt4.isHidden = true
        dirt4.alpha = 0
    }
    
    func randomTimeForDirtAppear() {
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(pickOneRandomDirt), userInfo: nil, repeats: true)
    }
    
    @objc func pickOneRandomDirt() {
        let allDirt = [1, 2, 3, 4]
        switch allDirt.randomElement() {
        case 1:
            if dirt1.isHidden && currentStatus != .dirt1 {
                dirt1.isHidden = false
                dirtArray.append(1)
                dirt1.fadeIn()
            } else {
                guard let index = dirtArray.firstIndex(where: {$0 == 1}) else { return }
                dirtArray.remove(at: index)
                pickOneRandomDirt()
            }
        case 2:
            if dirt2.isHidden && currentStatus != .dirt2 {
                dirt2.isHidden = false
                dirtArray.append(2)
                dirt2.fadeIn()
            } else {
                guard let index = dirtArray.firstIndex(where: {$0 == 2}) else { return }
                dirtArray.remove(at: index)
                pickOneRandomDirt()
            }
        case 3:
            if dirt3.isHidden && currentStatus != .dirt3 {
                dirt3.isHidden = false
                dirtArray.append(3)
                dirt3.fadeIn()
            } else {
                guard let index = dirtArray.firstIndex(where: {$0 == 3}) else { return }
                dirtArray.remove(at: index)
                pickOneRandomDirt()
            }
        case 4:
            if dirt4.isHidden && currentStatus != .dirt4 {
                dirt4.isHidden = false
                dirtArray.append(4)
                dirt4.fadeIn()
            } else {
                guard let index = dirtArray.firstIndex(where: {$0 == 4}) else { return }
                dirtArray.remove(at: index)
                pickOneRandomDirt()
            }
        default:
            break
        }
    }
    
    func moveUp() {
        labelStatus.text = "Moving Up"
        currentStatus = .dirt1
        vacuumCleanerCenterY.constant = 0
        movingAnimation()
    }
    
    func moveRight() {
        labelStatus.text = "Moving Right"
        currentStatus = .dirt2
        vacuumCleanerCenterX.constant = UIScreen.main.bounds.width - 200
        movingAnimation()
    }
    
    func moveDown() {
        labelStatus.text = "Moving Down"
        currentStatus = .dirt3
        vacuumCleanerCenterY.constant = viewSpaceForMoving.frame.height - 200
        movingAnimation()
    }
    
    func moveLeft() {
        labelStatus.text = "Moving Left"
        currentStatus = .dirt4
        vacuumCleanerCenterX.constant = 0
        movingAnimation()
    }
    
    func searchDirt() {
        switch currentStatus {
        case .dirt1:
            nextState(isDirty: !dirt1.isHidden)
        case .dirt2:
            nextState(isDirty: !dirt2.isHidden)
        case .dirt3:
            nextState(isDirty: !dirt3.isHidden)
        case .dirt4:
            nextState(isDirty: !dirt4.isHidden)
        }
    }
    
    func nextState(isDirty: Bool) {
        if isDirty {
            labelStatus.text = "Cleaning"
            cleanAndHiddenAnimation()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + (isDirty ? 1.5 : 1)) {
            switch self.currentStatus {
            case .dirt1:
                self.moveRight()
            case .dirt2:
                self.moveDown()
            case .dirt3:
                self.moveLeft()
            case .dirt4:
                self.moveUp()
            }
        }
    }
    
    func cleanAndHiddenAnimation() {
        vacuumCleaner.shake()
        switch self.currentStatus {
        case .dirt1:
            dirt1.fadeOut { (_) in
                self.dirt1.isHidden = true
            }
        case .dirt2:
            dirt2.fadeOut { (_) in
                self.dirt2.isHidden = true
            }
        case .dirt3:
            dirt3.fadeOut { (_) in
                self.dirt3.isHidden = true
            }
        case .dirt4:
            dirt4.fadeOut { (_) in
                self.dirt4.isHidden = true
            }
        }
    }
    
    func movingAnimation() {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveEaseInOut) {
            self.view.layoutIfNeeded()
        } completion: { (_) in
            self.searchDirt()
        }
    }
}
